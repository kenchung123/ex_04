﻿using System.Threading.Tasks;
using Example_04.Models;
using Example_04.Services;
using Microsoft.AspNetCore.Mvc;

namespace Example_04.Controllers
{
    public class ClassController : Controller
    {
        private readonly InterfaceGeneric<Class> _classRepository;
        public ClassController(InterfaceGeneric<Class> classRepository)
        {
            _classRepository = classRepository;
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Class Classes)
        {
            if (ModelState.IsValid)
            {
                _classRepository.Insert(Classes);
                _classRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Create));
        }
        public IActionResult Index()
        {
            return View( _classRepository.GetTEntity());
        }
//        public IActionResult Delete()
//        {
//            return View();
//        }
//        public IActionResult Update()
//        {
//            return View();
//        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        {
            return View(_classRepository.GetById(Id));
        }
//        public IActionResult Details()
//        {
//            return View();
//        }
        public IActionResult Delete(int? Id)
        {
           
            return View(_classRepository.GetById(Id));
        }
        // POST: Loggin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _classRepository.Delete(id);
            _classRepository.Save();
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public async Task<IActionResult>Edit(Class classes)
        {
            _classRepository.Update(classes);
            _classRepository.Save();
            return RedirectToAction(nameof(Index));
        }

    }
}