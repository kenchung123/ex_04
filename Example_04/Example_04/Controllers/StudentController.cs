﻿using System.Threading.Tasks;
using Example_04.Models;
using Example_04.Services;
using Microsoft.AspNetCore.Mvc;
namespace Example_04.Controllers
{
    public class StudentController : Controller
    {
        private readonly InterfaceGeneric<Student> _studentRepository;
        public StudentController(InterfaceGeneric<Student> studentRepository)
        {
            _studentRepository = studentRepository;
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Student Students)
        {
            if (ModelState.IsValid)
            {
                _studentRepository.Insert(Students);
                _studentRepository.Save();
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Create));
        }
        public IActionResult Index()
        {
            return View( _studentRepository.GetTEntity());
        }
//        public IActionResult Delete()
//        {
//            return View();
//        }
//        public IActionResult Update()
//        {
//            return View();
//        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        {
            return View(_studentRepository.GetById(Id));
        }
        public IActionResult Details()
        {
            return View();
        }
        public IActionResult Delete(int? Id)
        {
           
            return View(_studentRepository.GetById(Id));
        }
        // POST: Loggin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            _studentRepository.Delete(id);
            _studentRepository.Save();
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public async Task<IActionResult>Edit(Student student)
        {
            _studentRepository.Update(student);
            _studentRepository.Save();
            return RedirectToAction(nameof(Index));
        }
      
    }
}